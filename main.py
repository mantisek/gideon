import discord
import os
import dotenv
import GidEmbed

dotenv.load_dotenv()
intents = discord.Intents(messages = True, guilds = True, reactions = True, members = True, presences = True, message_content = True)
bot = discord.Bot(debug_guilds=[965262490973900800], intents=intents)

guild_ids = [965262490973900800]

@bot.event
async def on_ready():
    print(f"{bot.user} is ready and online!")

@bot.event
async def on_message(message):
    print(message.content)
    if message.content == "ping":
        await message.channel.send("pong")

@bot.slash_command(guild_ids=guild_ids)
async def fextra(ctx, query : discord.Option(str)):
    print(query)
    g = GidEmbed.GidEmbed(query)
    await ctx.respond("Here's your test embed",embed=g.embed)


bot.run(os.getenv('TOKEN')) # run the bot with the token